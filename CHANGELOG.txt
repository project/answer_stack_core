Answer Stack Core 8.x-1.x, xxxx-xx-xx
-------------------------------------

#3032727 by BramDriesen: Create initial module
#3032944 by BramDriesen: Create a vocabulary for question tags
#3032954 by BramDriesen: Programmatically create the node type "Question"
#3033121 by BramDriesen: Programmatically create the node type "Answer"