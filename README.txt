INTRODUCTION
------------

The Answer Stack Core module will provide the basic functionality needed to
ask questions and provide answers.

For a full description of the module, visit the project page:
  https://www.drupal.org/project/answer_stack_core
To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/answer_stack_core
For the complete distribution please visit the distribution page:
  https://www.drupal.org/project/answer_stack


REQUIREMENTS
------------

None.


RECOMMENDED MODULES
-------------------

None.


INSTALLATION
------------

It is recommended to install this module using composer.


CONFIGURATION
-------------

None for the moment.


TROUBLESHOOTING
---------------

If you encounter issues please create an issue in the issue queue at:
http://drupal.org/project/issues/answer_stack_core.


MAINTAINERS
-----------

Current maintainers:
 * Bram Driesen (BramDriesen) - https://www.drupal.org/u/BramDriesen
